angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {

        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })
    .state('app.info', {
        url: '/info',
        views: {
            'menuContent': {
                templateUrl: 'templates/info.html'
            }
        }
    })
    .state('app.registro', {
        url: '/registro',
        views: {
            'menuContent': {
                templateUrl: 'templates/registro.html'
            }
        }
    })
    .state('app.registro2', {
        url: '/registro2',
        views: {
            'menuContent': {
                templateUrl: 'templates/registro2.html'
            }
        }
    }) 
    .state('app.checkin', {
        url: '/checkin',
        views: {
            'menuContent': {
                templateUrl: 'templates/checkin.html'
            }
        }
    })   
    .state('app.bienvenidos', {
        url: '/bienvenidos',
        views: {
            'menuContent': {
                templateUrl: 'templates/bienvenidos.html'
            }
        }
    })
    .state('app.marketplace', {
        url: '/marketplace',
        views: {
            'menuContent': {
                templateUrl: 'templates/marketplace.html'
            }
        }
    })
    .state('app.marketplace2', {
        url: '/marketplace2',
        views: {
            'menuContent': {
                templateUrl: 'templates/marketplace2.html'
            }
        }
    })
    .state('app.marketplace3', {
        url: '/marketplace3',
        views: {
            'menuContent': {
                templateUrl: 'templates/marketplace3.html'
            }
        }
    })

    .state('app.chat', {
        url: '/chat',
        views: {
            'menuContent': {
                templateUrl: 'templates/chat.html'
            }
        }
    })

    .state('app.ordenes', {
        url: '/ordenes',
        views: {
            'menuContent': {
                templateUrl: 'templates/ordenes.html'
            }
        }
    })

    .state('app.ordenes2', {
        url: '/ordenes2',
        views: {
            'menuContent': {
                templateUrl: 'templates/ordenes2.html'
            }
        }
    })

    .state('app.perfil', {
        url: '/perfil',
        views: {
            'menuContent': {
                templateUrl: 'templates/perfil.html'
            }
        }
    })

    .state('app.perfil2', {
        url: '/perfil2',
        views: {
            'menuContent': {
                templateUrl: 'templates/perfil2.html'
            }
        }
    })

    .state('app.ajustes', {
        url: '/ajustes',
        views: {
            'menuContent': {
                templateUrl: 'templates/ajustes.html'
            }
        }
    })

    .state('app.comparte', {
        url: '/comparte',
        views: {
            'menuContent': {
                templateUrl: 'templates/comparte.html'
            }
        }
    }) 

    .state('app.historial', {
        url: '/historial',
        views: {
            'menuContent': {
                templateUrl: 'templates/historial.html'
            }
        }
    })  

    .state('app.acercade', {
        url: '/acercade',
        views: {
            'menuContent': {
                templateUrl: 'templates/acercade.html'
            }
        }
    })            
    ;

    $urlRouterProvider.otherwise('/app/info');
});
